Bitcoin Cash Node Technical Bulletin
--------------------------
 Date: December 18, 2021
 Author: matricz
 
---

## Summary

1. Introduction and rationale
2. The general architecture
3. `CConnman`'s threads
4. Appendix: Description of relevant classes and files

---

## Introduction and rationale

The aim of this technical bulletin is to describe the current inner workings of the network management of BCHN. 

Bitcoin Cash is a peer to peer network, so the network code is very important both from a functional and security viewpoint. 

The current incarnation of the network code (as of v24.0.0) is based on the general architecture set by the original Satoshi code in Bitcoin v0.1.0/v0.1.3 ([Reference](https://satoshi.nakamotoinstitute.org/code/)).

The rationale for this documentation is the assesment of the benefits and impact of changing the general architecture with one that is more parallel (wrt peer comunication). In the current implementation the receiving network thread "commands" the rest of the application; moreover only one incoming message at a time can be processed. Reengineering this part of the application can have important performance improvements. No specific reccomendations will be proposed in this report.

All of the following is based on BCHN v24.0.0 (at `1ef932b1b7`).

## The general architecture

The application has a gobally scoped `std::unique_ptr<CConnman> g_connman` which, besides managing connections, as the name suggests, manages most of the aspects of networking. It is started as the last step of initialization, after all other subsystems have are running. In doing so, the initialisation thread esentially passes the control of the application to the network threads.

A `CNode` represents a node/peer, and it contains all of the information regarding the connection to that peer, including current low-level data buffers and application-level network `CNetMessage`s. `g_connman` maintains a vector of `CNode`s/connected peers.

`Start`ing the `CConnman` will start the following threads:

* `net` running the `ThreadSocketHandler` function, which is responsible for net sockets reading and writing of messages.
* `msghand` running `ThreadMessageHandler` which handles messages received from peers.
* `addcon` running `ThreadOpenAddedConnections` and `opencon` running `ThreadOpenConnections` which manage connections to manually-added and network-announced peers.

There is a level of decoupling here in that the `net` thread writes to the `CNode`s' `vProcessMsg` message list and reads from the `CNode`s' `vSendMsg` message deque, while the `msghand` thread does the contrary, and they work in parallel. TODO: It currently escapes me why one is a list and the other a deque.

## `CConnman`'s threads

The `net` thread is pretty straightforward read/write loop for network sockets and for each iteration it: 
1. Does some initializations
2. Marks each node/peer as "selected for receiving", "selected for sending" or neither
3. Accepts connections from new nodes/peers, if any
4. Makes a copy of the `vNodes` vector (TODO why?); for each node in the copied vector
    1. if it was set for receiving, receive data and possibly transform it into `CNetMessage`s; add them to `vProcessMsg` (the node's list of messages to be processed). Also `WakeMessageHandler`.
    2. otherwise, if it was set for sending, `SocketSendData`
    3. an `InactivityCheck` is done for the node/peer

---

The `msghand` thread is a loop for application-level messages and it:
1. Makes a copy of the `vNodes` vector; for each node in the copied vector
    1. processes a single message from the incoming `vProcessMsg` list, if any (via `ProcessMessages`). Processing a message will often have the effect of sending some messages - for example when we recieve a `GETDATA`, we'll end up enqueing the requested data in this `CNode`'s `vSendMsg` (via `CConnman::PushMessage`). If any of the `CNode`s has any more messages in `vProcessMsg` then the `fMoreWork` flag will be set to true.
    2. does a `SendMessages`; this will maintain connection with the peer via `ping/pong`, exchange recent peers, manage IBD (?), send `inv`entories for recent blocks and transactions, send `getdata` for transactions we're missing, and some other housekeeping regarding this `CNode`
2. If `fMoreWork` has been set by any of the `CNode`s, the whole loop is repeated immidiately; else it waits for the closest poisson delay of the `CNode`s' `nNextInvSend` and repeats. Optionally, this thread can be woken up for processing with `WakeMessageHandler` (used by the `net` thread loop).

The `PeerLogicValidation::ProcessMessages` and `ProcessMessage` functions are really complex. They have a big part of the p2p fuctionality and application logic. 

Each `CNode` has a poisson delay set for sending `inv`s to it. When this delay expires for one node, all of the nodes will be processed again.


## Appendix: Description of relevant classes and files

*In the following `is a` strictly signfies that a class extends another. `has a` strictly signfies composition.*

A `CNetAddr` (in netaddress.h) is a network address (IPv6, or IPv4 using mapped IPv6 range (::FFFF:0:0/96)).

A `CService` (in netaddress.h) *is a* `CNetAddr` that also has a (TCP) port.

A `CAddress` (in protocol.h) *is a* `CService` "with information about it as peer" (ie `ServiceFlags`).

A `CAddrInfo` (in addrman.h) *is a* `CAddress` with "extended statistics". It is serializable and managed by `CAddrMan` (in addrman.h) which, among other things, is what reads and writes to the peers.dat file (via `CAddrDB`).

A `CSubNet` (in netaddress.h) *has a* `CNetAddr` and a `netmask`. It's used by the `BanMan` (in banman.h), via `CBanDB`, and also other parts of the application, like the http server

The `netbase.h` file has a number of functions for basic networking.

A `NetEventsInterface` (in net_processing.h) is an "interface for message handling". The `PeerLogicValidation`, the class that does most of the network processing (in net_processing.cpp) *is a* `NetEventsInterface` (and also *is a* `CValidationInterface`); it is the only class that implements this interface.

A `CNetMessage` (in net.h) contains partial or complete messages. While data is being received, the message can be left partial, until the next read. A complete message *has a* `CMessageHeader` and the `CDataStream`.

A `CNode` (in net.h) represents information about a network peer. It *has a* `CAddress` and data structures for exchange of messages and raw data. 

A `CConnman` (in net.h) manages the connections to peers. It *has a* vector of `CNode` pointers, and methods to connect, disconnect and otherwise manage connections and peers.

